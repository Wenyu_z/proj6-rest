import flask
from flask import Flask, request, Response
from flask_restful import Resource, Api
from pymongo import MongoClient
from collections import OrderedDict
import json
import os

# Instantiate the app
app = Flask(__name__)
api = Api(app)
client = MongoClient('mongodb:27017')

db = client.acp_times

def dict_to_csv(d):
    csv = ""
    for key, value in d.items():
        csv_row = f"<br>{key},{value}</br>"
        csv += csv_row 
    return csv

# Virtually all the logic between these resources is shared
# but I don't care to factor it out.


class ViewAllTimes(Resource):
    def get(self, format='json'):
        top = request.args.get('top')
        if top != None:  # I check twice. Not great.
            top = int(top)
        in_csv = 'csv' in format
        _items = db.times.find()
        items = [item for item in _items]
        items = sorted(items, key=lambda item: item['name'])
        if top == None or top > len(items):
            top = len(items)
        times = [f'open: {i["open"]}, close: {i["close"]}' for i in items]
        controls = [item['name'] for item in items]
        control_times = OrderedDict()
        for i, control in enumerate(controls[-top:]):
            control_times[control] = times[i]
        if in_csv:
            print(dict_to_csv(control_times))
            return Response(dict_to_csv(control_times))
        return flask.jsonify(control_times)

class ViewOnlyOpen(Resource):
    def get(self, format='json'):
        top = request.args.get('top')
        if top != None:  # I check twice. Not great.
            top = int(top)
        in_csv = 'csv' in format
        _items = db.times.find()
        items = [item for item in _items]
        items = sorted(items, key=lambda item: item['name'])
        if top == None or top > len(items):
            top = len(items)
        times = [f'open: {i["open"]}' for i in items]
        controls = [item['name'] for item in items]
        control_times = OrderedDict()
        for i, control in enumerate(controls[-top:]):
            control_times[control] = times[i]
        if in_csv:
            return Response(dict_to_csv(control_times))
        return flask.jsonify(control_times)


class ViewOnlyClosed(Resource):
    def get(self, format='json'):
        top = request.args.get('top')
        if top != None:  # I check twice. Not great.
            top = int(top)
        in_csv = 'csv' in format
        _items = db.times.find()
        items = [item for item in _items]
        items = sorted(items, key=lambda item: item['name'])
        if top == None or top > len(items):
            top = len(items)
        times = [f'close: {i["close"]}' for i in items]
        controls = [item['name'] for item in items]
        control_times = OrderedDict()
        for i, control in enumerate(controls[-top:]):
            control_times[control] = times[i]
        if in_csv:
            return Response(dict_to_csv(control_times))
        return flask.jsonify(control_times)


api.add_resource(ViewAllTimes, '/listAll/<string:format>', '/listAll')
api.add_resource(ViewOnlyClosed, '/listClosedOnly/<string:format>',
                '/listClosedOnly')
api.add_resource(ViewOnlyOpen, '/listOpenOnly/<string:format>',
                '/listOpenOnly')
# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5001, debug=True)

"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#


OPEN_TABLE = [(200,34),(400,32),(600,30),(1000,28)] #open table reference to max_speed

# CLOSE_TABLE = [(200,15),(400,15),(600,15),(1000,11.428)]#close table reference to min_speed


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    control_dist_km = round(control_dist_km)

    if control_dist_km == 0:
        return brevet_start_time
    if control_dist_km >brevet_dist_km:
        control_dist_km = brevet_dist_km
    time = arrow.get(brevet_start_time)
    remain_dist = control_dist_km
    prev = 0
    if control_dist_km <=200:
        seg_time = control_dist_km/34
        hr = int(seg_time)
        mt = round(60*(seg_time-hr))
        time = time.shift(hours=hr, minutes=mt)
    else:
        for seg_dist, speed in OPEN_TABLE:
            if remain_dist > 0 :
                dist = min(seg_dist-prev, remain_dist)
                seg_time = dist/speed
                hr = int(seg_time)
                mt = round(60*(seg_time-hr))
                time = time.shift(hours=hr, minutes=mt)
                remain_dist -=dist
                prev = seg_dist
    open_time = time.isoformat()

    return open_time


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    control_dist_km = round(control_dist_km)

    if control_dist_km == 0:
        return brevet_start_time

    time = arrow.get(brevet_start_time)

    if control_dist_km >=brevet_dist_km:
        if brevet_dist_km == 200:
            time = time.shift(hours=13, minutes=30)
        elif brevet_dist_km == 300:
            time = time.shift(hours=20, minutes=0)
        elif brevet_dist_km == 400:
            time = time.shift(hours=27, minutes=0)
        elif brevet_dist_km == 600:
            time = time.shift(hours=40, minutes=0)
        elif brevet_dist_km == 1000:
            time = time.shift(hours=75, minutes=0)
    else:
        if control_dist_km <600:
            seg_time = control_dist_km/15
            hr = int(seg_time)
            mt = round(60*(seg_time-hr))
            time = time.shift(hours=hr, minutes=mt)
        else:
            seg_time = (control_dist_km-600)/11.428
            hr = int(seg_time)
            mt = round(60*(seg_time-hr))
            time = time.shift(hours=hr+40, minutes=mt)

    close_time=time.isoformat()

    return close_time

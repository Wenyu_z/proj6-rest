import os
import flask
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient
import acp_times

app = Flask(__name__)
client = MongoClient('mongodb:27017')

db = client.acp_times


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")

    return render_template('calc.html')

@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    return render_template('404.html'), 404

@app.route('/_submit_times')
def print_controls():
    """"
    When sent a JSON object from calc.html specifying brevets and controls,
    this function stores this information in a the database, along with the
    open and close times for each brevet.
    """
    db.times.drop()
    db.times.drop()
    brevet = request.args.get('brevet', type=int)
    datetime = request.args.get('datetime', type=str)
    controls = request.args.get('controls', type=str)
    print(controls)
    controls = eval(controls) # Not safe!
    controls = sorted([int(round(float(x))) for x in controls])
    if not controls:
        return flask.jsonify(result="No control error")
    if any([x for x in controls if x > (1.2 * brevet)]):
        return flask.jsonify(result="Control too big error")
    if any([x for x in controls if x < 0]):
        return flask.jsonify(result="Control below zero error")

    db.brevet.insert_one({'name': 'brevet', 'description': 'brevet',
                         'kms': brevet})
    for control in controls:
        name = f'{str(control) + "kms"}'
        open_time = acp_times.open_time(control, brevet, datetime)
        close_time = acp_times.close_time(control, brevet, datetime)
        db.times.insert_one({'name': name, 'description': 'times',
                             'open': open_time, 'close': close_time})
        for item in db.times.find():
            print(item['name'])
            if 'control' in item['name']:
                print(item['close'])
            elif item['name'] == 'brevet':
                print(item['kms'])
    return flask.jsonify(result="Success")


"""
I did not realize I would still need to populate the open and close fields in
calc.html, hence the mess this turned into. I'll refactor this if I find the
time.
"""


@app.route('/_calc_times')
def _calc_times():

    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    brevet = request.args.get('brevet', type=int)
    datetime = request.args.get('datetime', type=str)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    open_time = acp_times.open_time(km, brevet, datetime)
    close_time = acp_times.close_time(km, brevet, datetime)
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)


@app.route('/_view_times')
def view():
    _items = db.times.find()
    items = [item for item in _items]
    return render_template('todo.html', items=items)


#@app.route('/<path: path>'):
#   cwd = os.getcwd()
#    if not os.path.isfile(cwd + '/templates/' + path):
#        return '<html> <head> 404 </head> </html>', 404
#    return render_template(path)

@app.route('/invalid')
def invalid():
    return render_template('invalid.html')


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
